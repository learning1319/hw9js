/* 1) const element = document.createElement("тег")
2) Визначає положення елемента (а) що вставляється, відносно елемента(в) до якого застосовується функція. 
beforebegin - вставить "а" перед "в"; 
afterbegin - "а" всередиині "в" і він буде першим в його вмісті,
 beforeend - "а" всередині "в", але кінці вмісту, 
 afterend - "а" одразу після "в"
3) element.remove
*/

function newList (array, parent = document.body) {

const list = document.createElement('ul');

parent.append(list);

array.map((currentValue) => {
   let newListItem = document.createElement('li');
   newListItem.textContent =`${currentValue}`;
   list.append(newListItem);
})
}
let city = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
newList(city)